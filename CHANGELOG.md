# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.2.0

- minor: Add support for ROLE_ARN parameter.

## 2.1.0

- minor: Internal maintenance: make own Dockerfile independent of kubectl-run pipe.
- minor: Upgrade kubectl lib 1.19 -> 1.21. See kubernetes release notes in https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html.

## 2.0.0

- major: Update base dependency kubectl-run to version 3.0.1: Bumped AWS CLI v1 -> v2, bitbucket-pipes-toolkit -> 2.2.0

## 1.4.2

- patch: Internal maintenance: Bump test requirement bitbucket-pipes-toolkit -> 2.2.0.

## 1.4.1

- patch: Fix bug with LABELS variable. Labels are intended to be used to specify identifying attributes of Kubernetes objects.

## 1.4.0

- minor: Bump kubectl-run pipe 1.1.1-> 1.3.0.

## 1.3.1

- patch: Internal maintenance: bump pipe-release.

## 1.3.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.2.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.2.3

- patch: Internal maintenance: Add gitignore secrets.

## 1.2.2

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.2.1

- patch: Added more details about Kubernetes access controls to the README
- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 1.2.0

- minor: Add default values for AWS variables.

## 1.1.1

- patch: Internal maintenance: Add check for newer version.

## 1.1.0

- minor: The pipe now accepts directory path in RESOURCE_PATH variable

## 1.0.0

- major: Changed the SPEC_FILE varialbe to RESOURCE_PATH

## 0.2.1

- patch: Documentation improvements.

## 0.2.0

- minor: Fixed the docker image reference

## 0.1.0

- minor: Initial release
- patch: Updated the base pipe version

