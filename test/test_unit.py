import os
import sys
from copy import copy
from unittest import mock, TestCase

from pipe.pipe import EKSDeployPipe, schema


class EKSTestCase(TestCase):
    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    @mock.patch.dict(os.environ, {
        'AWS_ACCESS_KEY_ID': 'test_key',
        'AWS_SECRET_ACCESS_KEY': 'test_secret_key',
        'AWS_DEFAULT_REGION': 'default',
        'KUBECTL_COMMAND': 'test_command',
        'CLUSTER_NAME': 'test-cluster',
        'ROLE_ARN': 'test-role'
    })
    @mock.patch('subprocess.run')
    def test_eks_run_with_role_arn(self, subprocess_mock):
        subprocess_mock.return_value = mock.Mock(returncode=0)

        EKSDeployPipe(schema=schema, check_for_newer_version=True).configure()

        subprocess_mock.assert_called_once_with(
            ['aws', 'eks', 'update-kubeconfig', '--name=test-cluster', '--role-arn=test-role'], stdout=sys.stdout)
